#!/bin/sh

cd /volume2/series/new_episode

clear
echo -e "\n"
# date +%Y/%m/%d-%Hh%M
# date -u
date -I'seconds' | sed -e 's/T/\ /g' | sed -e 's/+.*$//g'
echo -e "\n"

if ls *.html > /dev/null 2>&1 ; 
then 
	echo "html file(s) -- DELETING"
	rm *.html; 
	else echo "No html file(s)"  ; 
fi

if ls */*.html > /dev/null 2>&1 ; 
then 
	rm */*.html; 
fi

if ls *.txt > /dev/null 2>&1 ; 
then 
	echo "txt file(s) -- DELETING"
	rm *.txt; 
	else echo "No txt file(s)"; 
fi

if ls */*.txt > /dev/null 2>&1 ; 
then 
	rm */*.txt;
fi

# Suppression des fichiers "parasites" qui arrivent pendant le DL
cd ..
if ls */*/*.url > /dev/null 2>&1;
then
#echo il y en a putain 
rm */*/*.url
#else 
#echo il NY EN A PAS  
fi

if ls */*/*/*.url > /dev/null 2>&1;
then
#echo il y en a putain 
rm */*/*/*.url
#else 
#echo il NY EN A PAS  
fi
cd new_episode
#################################################################

# echo -e "\n"
########

# sauvegarde du séparateur de champ
old_IFS=$IFS  				
# nouveau séparateur de champ, le caractère fin de ligne      
IFS=$'\n'

### Se logguer avec un utilisateur différent, pour pouvoir garder l'authentification en 2 étapes sur le principal
id='rpi_salon'
pwd='XXXshooter83XXX./*'
#####################

# Cette partie nous procure un sid pour pouvoir avoir une session logguée sur DownloadStation de façon à pouvoir utiliser l'api
###### Ici on se loggue et on enregistre la réponse à la requête dans sid.txt
#<<<<<<< HEAD
#=======
# curl -kso sid.txt "http://109.24.170.218:5000/webapi/auth.cgi?api=SYNO.API.Auth&version=2&method=login&account=admin&passwd=***MyPassword***&session=DownloadStation&format=sid"
#>>>>>>> de3b0a94bb780694d882defb392ea1e48f3a190b
curl -kso sid.txt "http://127.0.0.1:5000/webapi/auth.cgi?api=SYNO.API.Auth&version=2&method=login&account=${id}&passwd=${pwd}&session=DownloadStation&format=sid"
# curl -kso sid.txt "http://109.24.170.218:5000/webapi/auth.cgi?api=SYNO.API.Auth&version=2&method=login&account=admin&passwd=ghysgab2009&session=DownloadStation&format=sid"
 ###### Ici on récupère le sid contenu dans sid.txt --> qui sera utile pour pouvoir utiliser l'API (lancement de DL etc...)
sid=`cat sid.txt | grep -o 'sid".*"}' | sed -e 's/sid\":\"//g' | sed -e 's/\"}//g'`
# echo "sid: $sid"
echo -e "\n"
############################

###############################################################################################################################################################
## Partie qui gère les notifications en regroupant les DL du meme fichier
curl -ks "http://127.0.0.1:5000/webapi/DownloadStation/task.cgi?api=SYNO.DownloadStation.Task&version=1&method=list&additional=detail,&_sid=$sid" > list_rough.txt

## Affiche toutes les destinations 
cat list_rough.txt | sed -e 's/{"additional":/\n/g' | sed -e 's/^.*"destination":/destination:/g' | sed -e 's/\",\"/\n/g' | grep '^destination' | sed -e 's/destination:\"//g' | sed -e 's/\ /\_/g' | grep 'series' | uniq > destinations.txt
# cat list_rough.txt | sed -e 's/{"additional":/\n/g' | sed -e 's/^.*"destination":/destination:/g' | sed -e 's/"priority".*"status"/status/g' | sed -e 's/"status_extra".*"title"/title/g' | sed -e 's/,"type".*"admin"},//g' | sed -e 's/ /\_/g' | sed '1d' | sed -e 's/,"type".*true}//g' | grep series/ | sed -e 's/",status.*$//g' | sed -e 's/destination:"//g' | uniq > destinations.txt
cat list_rough.txt | sed -e 's/{"additional":/\n/g' | sed -e 's/^.*"destination":/destination:/g' | grep -v 'finished' | sed -e 's/\",\"/\n/g' | grep '^destination' | sed -e 's/destination:\"//g' | sed -e 's/\ /\_/g' | grep 'series' | uniq > destinations_downloading.txt
# cat list_rough.txt | sed -e 's/{"additional":/\n/g' | grep -v "finished" | sed -e 's/^.*"destination":/destination:/g' | sed -e 's/"priority".*"status"/status/g' | sed -e 's/"status_extra".*"title"/title/g' | sed -e 's/,"type".*"admin"},//g' | sed -e 's/ /\_/g' | sed '1d' | sed -e 's/,"type".*true}//g' | grep series/ | sed -e 's/",status.*$//g' | sed -e 's/destination:"//g' | uniq > destinations_downloading.txt
## token utilisé par l'api pushbullet
token='8TigDjKEbc5RT9BWUaeDzsYGwhAb4Hnz'
token_gab='s7zdjlUnhI8iu4aEzHKAmOHlbYFG5Dw9'
token_paul='o.Of7obRvKKQozhXVFqgQdGmBT8zRVfX4b'

## Clear la liste et la met dans liste_clear.txt
# cat list_rough.txt | sed -e 's/{"additional":/\n/g' | sed -e 's/^.*"destination":/destination:/g' | sed -e 's/"priority".*"status"/status/g' | sed -e 's/"status_extra".*"title"/title/g' | sed -e 's/,"type".*"admin"},//g' | sed -e 's/ /\_/g' | sed '1d' | sed -e 's/,"type".*true}//g' | grep series/ > list_clear.txt
# cat list_rough.txt | sed -e 's/{"additional":/\n/g' | sed -e 's/^.*"destination":/destination:/g' | sed -e 's/"priority".*"id"/id/g' | sed -e 's/"status_extra".*"title"/title/g' | sed -e 's/,"type".*"admin"},//g' | sed -e 's/ /\_/g' | sed '1d' | sed -e 's/,"type".*true}//g' | grep series/ > list_clear.txt
# cat list_rough.txt | sed -e 's/{"additional":/\n/g' | sed -e 's/^.*"destination":/destination:/g' | sed -e 's/"priority".*"id"/id/g' |sed -e 's/,"size".*"status"/,status/g' | sed -e 's/"status_extra".*"title"/title/g' | sed -e 's/,"type".*"admin"},//g' | sed -e 's/ /\_/g' | sed '1d' | sed -e 's/,"type".*true}//g' | grep series/ > list_clear.txt
cat list_rough.txt | sed -e 's/{"additional":/\n/g' | sed -e 's/^.*"destination":/destination:/g' | sed -e 's/\"seedelapsed\".*\"id\"/\"id\"/g' | sed -e 's/\ /\_/g' | grep 'series' > list_clear.txt
nbrDLDone=0;

for destination in `cat destinations.txt`
do 
	destination_to_compare=`echo ${destination} | sed -e 's/series\///g' | sed -e 's/\/.*$//g' | tr '[A-Z]' '[a-z]' | sed -e 's/_/-/g'`
	cat list_clear.txt | grep `echo $destination` | grep -o '\"status\":\".*\"' | sed -e 's/",.*$//g' | sed -e 's/"//g' > status_of_downloads.txt
	# cat list_clear.txt | grep `echo $destination` | grep -o 'status:.*' | sed -e 's/",.*$//g' | sed -e 's/"//g' > status_of_downloads.txt
	nbr_dl=`cat status_of_downloads.txt | wc -l`
	
	bool_dl_finished=`cat status_of_downloads.txt | grep -v 'finished' | wc -l`
	## Booléen pour savoir quand un DL est finit, en regroupant les DL pour une meme destination -> une seule notif
	if [ "$bool_dl_finished" = "0" ]
	then
		## Ici ajouter la requete pour une notification !
		#which_dl_is_done=`echo $destination | sed -e 's/series\/.*\///g' | sed -e 's/_/ /g'`
		tv_show_done_name=`echo $destination | sed -e 's/series\///g' | sed -e 's/\/.*$//g' | sed -e 's/_/ /g'`
		wich_episode_is_done=`echo $destination | sed -e 's/^.*\///g' | grep -o '[Ee][0-9][0-9]'`
		season_nbr=`echo $destination | sed -e 's/^.*saison_//g' | sed -e 's/\/.*$//g'`
		to_disp_in_notif=`echo ${tv_show_done_name} S${season_nbr}${wich_episode_is_done}`
		echo $to_disp_in_notif

		id=`cat tmdb_ids.dat | grep $tv_show_done_name | grep -o [0-9].*`
		episode_tmdb=`echo $wich_episode_is_done | grep -o [0-9][0-9]`
		base_url_img='https://image.tmdb.org/t/p/w780'
		response=`curl -s http://api.themoviedb.org/3/tv/$id/season/$season_nbr/episode/$episode_tmdb/images?api_key=7d7d89a7c475b8fdc9a5203419cb3964`
		img_url=`echo $response | tr "," "\n" | grep file_path | grep -o "/.*.jpg" | head -n 1`
		img_url_full=`echo $base_url_img$img_url`
		img_show_no_avail=`cat images_notif_wo_tmdb.dat | grep $tv_show_done_name | grep -o 'http.*$'`

		# if echo $response | grep -q '\"stills\":\[\]}' ;
		if echo $response | grep -q 'aspect_ratio' ;
		then
				echo IMAGE AVAILABLE

				if grep -q $destination_to_compare links_ghys.dat ;
				then
					curl -s -u "$token:" -kX POST https://api.pushbullet.com/v2/pushes --header 'Content-Type: application/json'  --data-binary '{"type": "file", "title": "Téléchargement terminé !", "body": "'"${to_disp_in_notif}"'", "file_type": "image/jpeg", "file_url": "'"${img_url_full}"'"}' > /dev/null 2>&1 &
					echo $which_dl_is_done notified to ghys
				fi	
				
				if grep -q $destination_to_compare links_gab.dat ;
				then 
					curl -s -u "$token_gab:" -kX POST https://api.pushbullet.com/v2/pushes --header 'Content-Type: application/json'  --data-binary '{"type": "file", "title": "Téléchargement terminé !", "body": "'"${to_disp_in_notif}"'", "file_type": "image/jpeg", "file_url": "'"${img_url_full}"'"}' > /dev/null 2>&1 &
					echo $which_dl_is_done notified to gab
				fi
				
				if grep -q $destination_to_compare links_paul.dat ;
				then
					curl -s -u "$token_paul:" -kX POST https://api.pushbullet.com/v2/pushes --header 'Content-Type: application/json'  --data-binary '{"type": "file", "title": "Téléchargement terminé !", "body": "'"${to_disp_in_notif}"'", "file_type": "image/jpeg", "file_url": "'"${img_url_full}"'"}' > /dev/null 2>&1 &
					echo $which_dl_is_done notified to paul
				fi
		else
				echo NO IMAGE available
				if grep -q $destination_to_compare links_ghys.dat ;
				then
					curl -s -u "$token:" -kX POST https://api.pushbullet.com/v2/pushes --header 'Content-Type: application/json'  --data-binary '{"type": "file", "title": "Téléchargement terminé !", "body": "'"${to_disp_in_notif}"'", "file_type": "image/jpeg", "file_url": "'"${img_show_no_avail}"'"}' > /dev/null 2>&1 &
					echo $which_dl_is_done notified to ghys
				fi	
				
				if grep -q $destination_to_compare links_gab.dat ;
				then 
					curl -s -u "$token_gab:" -kX POST https://api.pushbullet.com/v2/pushes --header 'Content-Type: application/json'  --data-binary '{"type": "file", "title": "Téléchargement terminé !", "body": "'"${to_disp_in_notif}"'", "file_type": "image/jpeg", "file_url": "'"${img_show_no_avail}"'"}' > /dev/null 2>&1 &
					echo $which_dl_is_done notified to gab
				fi

				if grep -q $destination_to_compare links_paul.dat ;
				then
					curl -s -u "$token_paul:" -kX POST https://api.pushbullet.com/v2/pushes --header 'Content-Type: application/json'  --data-binary '{"type": "file", "title": "Téléchargement terminé !", "body": "'"${to_disp_in_notif}"'", "file_type": "image/jpeg", "file_url": "'"${img_show_no_avail}"'"}' > /dev/null 2>&1 &
					echo $which_dl_is_done notified to paul
				fi
		fi

		# echo $which_dl_is_done notified
		# curl -s -u "$token:" -kX POST https://api.pushbullet.com/v2/pushes --header 'Content-Type: application/json'  --data-binary '{"type": "note", "title": "Téléchargement terminé !", "body": "'"${which_dl_is_done}"'", "deviceParams": "Galaxy S4"}' > /dev/null 2>&1 &

		##
		
		cat list_clear.txt | grep "$destination" | grep -o 'dbid_[0-9]*' > idDLToSuppr.txt
		## Ici ajouter la requête pour supprimer les DL qui correspondent
		for id in `cat idDLToSuppr.txt`
		do
			echo deleted
			curl -ks "http://127.0.0.1:5000/webapi/DownloadStation/task.cgi?api=SYNO.DownloadStation.Task&version=1&method=delete&id=$id&_sid=$sid" > /dev/null 2>&1 &
		done
		#################################################################
		nbrDLDone=`expr $nbrDLDone + 1`
		
	fi
done
if [ "$nbrDLDone" != "0" ]
then 
	##### Placer l'actualisation de la médiathèque vidéo de KODI ici

	# Pour lancer la synchro kodi: 
	curl -s --data-binary '{ "jsonrpc": "2.0", "method": "VideoLibrary.Scan", "id": "mybash"}' -H 'content-type: application/json;' http://admin:ghysgab2009@192.168.1.72:8080/jsonrpc > /dev/null 2>&1 &
	#
	# Pour clear meiatheque kodi: 
	#curl -s --data-binary '{ "jsonrpc": "2.0", "method": "VideoLibrary.Clean", "id": "mybash"}' -H 'content-type: application/json;' http://192.168.1.72:8080/jsonrpc &
echo Updating kodi

################################################################
fi
## Fin partie qui gère les notifications
###############################################################################################################################################################

# echo -e "\n"
# echo Bypassing CloudFlare...
# ./cloudflare_bypass.py > cookie.txt
# cookie=`cat cookie.txt | sed -n '1 p'`
# user_agent=`cat cookie.txt | sed -n '2 p'`

# echo $cookie 
# echo $user_agent

echo -e "\n"

for links in `cat links.dat`
do 
	name=`echo $links | sed -e 's/http:\/\/rmz.cr\///g' | sed -e 's/\/s//g' | sed -e 's/-[0-9][0-9][0-9][0-9]//g'`
	# name=`echo $links | sed -e 's/http:\/\/rapidmoviez.com\///g' | sed -e 's/\/s//g' | sed -e 's/-[0-9][0-9][0-9][0-9]//g'`
	name="$name.html"
	### Téléchargement des pages principales des séries sur rapidmoviez
	# wget -qO $name $links &
	# curl -s $links -H 'Accept-Encoding: gzip, deflate, sdch' -H 'Accept-Language: fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.49 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Cache-Control: max-age=0' -H 'Cookie: HstCfa1606961=1444751039969; HstCmu1606961=1444751039969; visid_incap_633716=nHa/6/5RRe2ZyDfQKYaxfj4mK1YAAAAAQUIPAAAAAAB6dWlgPwueRwhhBRL2mKxq; __cfduid=de1f5c9e83d8a6ed4b8e0302a6c3ca4b81446566037; c_ref_1606961=https%3A%2F%2Fwww.google.fr%2F; cf_clearance=d346374a503e6e73f39f383a91961c572773f269-1446975518-28800; __atuvc=11%7C41%2C36%7C42%2C3%7C43%2C11%7C44%2C9%7C45; rms3=a%3A5%3A%7Bs%3A10%3A%22session_id%22%3Bs%3A32%3A%228bb6b5972bdbed83423dce87daaa4bd6%22%3Bs%3A10%3A%22ip_address%22%3Bs%3A7%3A%220.0.0.0%22%3Bs%3A10%3A%22user_agent%22%3Bs%3A114%3A%22Mozilla%2F5.0+%28Windows+NT+10.0%3B+Win64%3B+x64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F47.0.2526.35+Safari%2F537.36%22%3Bs%3A13%3A%22last_activity%22%3Bi%3A1446995193%3Bs%3A9%3A%22user_data%22%3Bs%3A0%3A%22%22%3B%7Dca01619a8f972f6aa8213db0298ae4f04d3ce3c2; HstCla1606961=1446995204086; HstPn1606961=1; HstPt1606961=98; HstCnv1606961=16; HstCns1606961=20; _ga=GA1.2.1218672470.1444751040; _gat=1' -H 'Connection: keep-alive' --compressed > $name
	
	# Use the line below with the bypass cloudflare script...
	# curl -s $links -H "$cookie" -H "$user_agent" > $name
	curl -s $links > $name

	###
	echo Donwloading informations for $name ok | sed -e 's/-/\ /g' | sed -e 's/\.html//g'
done
wait
#########################

# # sauvegarde du séparateur de champ
# old_IFS=$IFS  				
# # nouveau séparateur de champ, le caractère fin de ligne      
# IFS=$'\n'                         

cd ..

path=`pwd`

# echo -e "\n"

for shows in `ls -d */ | grep -v '#.*' | grep -v '@' | grep -v 'new_episode'`
do  
	tv_shows=`echo $shows`
	echo -e "\n"
	echo $tv_shows |sed -e 's/\///g'
	html=`echo $tv_shows | sed -e 's/\///g' | sed -e 's/\ ([0-9][0-9][0-9][0-9])//g'  | sed -e 's/ /-/g'| tr [A-Z] [a-z]`
	cd $tv_shows

	## Pour gagner un peu de temps, a voir plus tard
	# num_saison=`echo ls -d */ | cut -c8,9 | sort -n | sed -n '$p'`
	# saison="saison $num_saison"
	
	for saisons in `ls -d */`
	do 
		num_saison=`echo $saisons | cut -c8,9 | sort -n`
		saison=`echo $saisons | sed "s/.$//"`
	done
	 
	cd $saison
	 
	num_episode=`find /volume2/series/$tv_shows$saison -type f | grep -o '[Ee][0-9][0-9]' | cut -c2,3 | sort -n | sed -n '$p'`

	if [ "$num_episode" = "08" ]
		then num_episode=8
	fi
	if [ "$num_episode" = "09" ]
		then num_episode=9
	fi

	num_episode_next=`echo $(( $num_episode+1 ))`

	# pour avoir le numéro de l'épisode sous la forme qui va bien qui apparait dans les liens
	num_episode_next=`printf '%02d' $num_episode_next`
	echo "Last -> S${num_saison} E${num_episode} | To DL -> S${num_saison} E${num_episode_next}" 

	cd ../..

	# Ici les pages des épisodes sont maintenant différenciées en fonction de la série (Pour sélection de la qualité future)
	tvShowsForEpisodePage=`echo $tv_shows | sed -e 's/\///g' | sed -e 's/\ /_/g'`
	namePageEpisode="${tvShowsForEpisodePage}_s${num_saison}e${num_episode_next}"
	tout="s${num_saison}.e${num_episode_next}"
	a_dl=`echo $tout | sed -e 's/\.//g'`

	if [ -f "new_episode/$html.html" ];
	then
		# Ici on prend en premier les 720p (classés par taille, on prend les plus gros en premier), en deuxième les 1080p et en dernier les autres que 480p (HDTV quoi)
		#sed -n '/<h2 style=\"font-size:28px;\">Episodes<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | sed -n '/<a href=\"/,/<\/a>/p' | sed -e 's/<\/a>/\n/g' | sed -e 's/<span .*href=\"//g' | sed -e 's/<a href=\"//g' | sed -e 's/<li>//g' | sed -e 's/\">/ /g' | grep UL | grep -v x265 | grep 720p | grep -o '/.*m=s' | grep "$a_dl" | uniq  >> new_episode/${namePageEpisode}_pages_links.txt
		# sed -n '/<h2 style=\"font-size:28px;\">Episodes<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | sed -n '/<a href=\"/,/<\/a>/p' | sed -e 's/<\/a>/\n/g' | sed -e 's/<span .*href=\"//g' | grep UL | grep -v x265 | grep 720p | grep "$a_dl" | sed -e 's/m=s\">.*(/m=s\">(/g' | sed -e 's/^\ *\//\//g' | sed -e 's/\">(/\ (/g' | awk '{print $2,$1}' | sed -e 's/GB)/00/g' | sed -e 's/MB//g' | sed -e 's/^(//g' | sed -e 's/\.//g' | sed -e 's/)//g' | sort -nr | sed -e 's/^.*\ //g' | sed -e 's/\".*$//g' > new_episode/${namePageEpisode}_pages_links.txt
		# sed -n '/<h2 style=\"font-size:28px;\">Episodes<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | sed -n '/<a href=\"/,/<\/a>/p' | sed -e 's/<\/a>/\n/g' | sed -e 's/<span .*href=\"//g' | sed -e 's/<a href=\"//g' | sed -e 's/<li>//g' | sed -e 's/\">/ /g' | grep UL | grep -v x265 | grep 1080p | grep -o '/.*m=s' | grep "$a_dl" | uniq  | sed -e 's/\".*$//g' >> new_episode/${namePageEpisode}_pages_links.txt
		# sed -n '/<h2 style=\"font-size:28px;\">Episodes<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | sed -n '/<a href=\"/,/<\/a>/p' | sed -e 's/<\/a>/\n/g' | sed -e 's/<span .*href=\"//g' | sed -e 's/<a href=\"//g' | sed -e 's/<li>//g' | sed -e 's/\">/ /g' | grep UL | grep -v x265 | grep HDTV | grep -o '/.*m=s' | grep "$a_dl" | grep -v '720p\|1080p' | uniq  | sed -e 's/\".*$//g' >> new_episode/${namePageEpisode}_pages_links.txt
		
		# Deuxième façon de prendre en premier les 720p (les plus gros en premier), les 1080p en seconds (les plus petits en premiers) et les 480p en derniers (les plus gros en premiers) 
		sed -n '/<h2 style=\"font-size:28px;\">Episodes<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | sed -n '/<a href=\"/,/<\/a>/p' | sed -e 's/<\/a>/\n/g' | sed -e 's/<span .*href=\"//g' | grep UL | grep -v x265 | grep 720p | grep "$a_dl" | sed -e 's/m=s\">.*(/m=s\">(/g' | sed -e 's/^\ *\//\//g' | sed -e 's/\">.*(/\ (/g' | awk '{print $2,$1}' | sed -e 's/\([0-9]\.[0-9]\)GB/\100/g' | sed -e 's/\([0-9]\)GB/\1000/g' | sed -e 's/\.//g' | sed -e 's/(\([0-9].*\))/\1/g' | sort -nr | sed -e 's/^.*\ //g' | sed -e 's/#.*$//g' | grep '^/' > new_episode/${namePageEpisode}_pages_links.txt
		sed -n '/<h2 style=\"font-size:28px;\">Episodes<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | sed -n '/<a href=\"/,/<\/a>/p' | sed -e 's/<\/a>/\n/g' | sed -e 's/<span .*href=\"//g' | grep UL | grep -v x265 | grep 1080p | grep "$a_dl" | sed -e 's/m=s\">.*(/m=s\">(/g' | sed -e 's/^\ *\//\//g' | sed -e 's/\">.*(/\ (/g' | awk '{print $2,$1}' | sed -e 's/\([0-9]\.[0-9]\)GB/\100/g' | sed -e 's/\([0-9]\)GB/\1000/g' | sed -e 's/\.//g' | sed -e 's/(\([0-9].*\))/\1/g' | sort -n | sed -e 's/^.*\ //g' | sed -e 's/#.*$//g' | grep '^/' >> new_episode/${namePageEpisode}_pages_links.txt
		sed -n '/<h2 style=\"font-size:28px;\">Episodes<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | sed -n '/<a href=\"/,/<\/a>/p' | sed -e 's/<\/a>/\n/g' | sed -e 's/<span .*href=\"//g' | grep UL | grep -v x265 | grep hdtv | grep -v 720p | grep -v 1080p | grep "$a_dl" | sed -e 's/m=s\">.*(/m=s\">(/g' | sed -e 's/^\ *\//\//g' | sed -e 's/\">.*(/\ (/g' | awk '{print $2,$1}' | sed -e 's/\([0-9]\.[0-9]\)GB/\100/g' | sed -e 's/\([0-9]\)GB/\1000/g' | sed -e 's/\.//g' | sed -e 's/(\([0-9].*\))/\1/g' | sort -nr | sed -e 's/^.*\ //g' | sed -e 's/#.*$//g' | grep '^/' >> new_episode/${namePageEpisode}_pages_links.txt


		# Partie qui cherche les Episodes SI il n'y a que des Episodes dans la catégorie "Archived"
		if [ `sed -n '/<h2 style=\"font-size:28px;\">Episodes<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | wc -l` == '0' ] && [ `sed -n '/<h2 style=\"font-size:28px;\">Archived<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | wc -l` != '0' ] ;
		then
			sed -n '/<h2 style=\"font-size:28px;\">Archived<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | sed -n '/<a href=\"/,/<\/a>/p' | sed -e 's/<\/a>/\n/g' | sed -e 's/<span .*href=\"//g' | grep UL | grep -v x265 | grep 720p | grep "$a_dl" | sed -e 's/m=s\">.*(/m=s\">(/g' | sed -e 's/^\ *\//\//g' | sed -e 's/\">.*(/\ (/g' | awk '{print $2,$1}' | sed -e 's/\([0-9]\.[0-9]\)GB/\100/g' | sed -e 's/\([0-9]\)GB/\1000/g' | sed -e 's/\.//g' | sed -e 's/(\([0-9].*\))/\1/g' | sort -nr | sed -e 's/^.*\ //g' | sed -e 's/#.*$//g' | grep '^/' > new_episode/${namePageEpisode}_pages_links.txt
			sed -n '/<h2 style=\"font-size:28px;\">Archived<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | sed -n '/<a href=\"/,/<\/a>/p' | sed -e 's/<\/a>/\n/g' | sed -e 's/<span .*href=\"//g' | grep UL | grep -v x265 | grep 1080p | grep "$a_dl" | sed -e 's/m=s\">.*(/m=s\">(/g' | sed -e 's/^\ *\//\//g' | sed -e 's/\">.*(/\ (/g' | awk '{print $2,$1}' | sed -e 's/\([0-9]\.[0-9]\)GB/\100/g' | sed -e 's/\([0-9]\)GB/\1000/g' | sed -e 's/\.//g' | sed -e 's/(\([0-9].*\))/\1/g' | sort -n | sed -e 's/^.*\ //g' | sed -e 's/#.*$//g' | grep '^/' >> new_episode/${namePageEpisode}_pages_links.txt
			sed -n '/<h2 style=\"font-size:28px;\">Archived<\/h2>/,/<div class=\"clear\">/p' new_episode/$html.html | sed -n '/<a href=\"/,/<\/a>/p' | sed -e 's/<\/a>/\n/g' | sed -e 's/<span .*href=\"//g' | grep UL | grep -v x265 | grep hdtv | grep -v 720p | grep -v 1080p | grep "$a_dl" | sed -e 's/m=s\">.*(/m=s\">(/g' | sed -e 's/^\ *\//\//g' | sed -e 's/\">.*(/\ (/g' | awk '{print $2,$1}' | sed -e 's/\([0-9]\.[0-9]\)GB/\100/g' | sed -e 's/\([0-9]\)GB/\1000/g' | sed -e 's/\.//g' | sed -e 's/(\([0-9].*\))/\1/g' | sort -nr | sed -e 's/^.*\ //g' | sed -e 's/#.*$//g' | grep '^/' >> new_episode/${namePageEpisode}_pages_links.txt
		fi
		#

		# base="http://rapidmoviez.com"
		# Changing the base...
		base="http://rmz.cr"

		# Nouvelle manière de dire si il n'y a pas d'épisodes dispo
		nbrPagesForEpisode=`cat new_episode/${namePageEpisode}_pages_links.txt | wc -l`

		if [ "$nbrPagesForEpisode" = "0" ]
			then echo "No new episode available"
		else 

			destination_dir=`echo $tv_shows S${num_saison}E${num_episode_next} | sed -e 's/\///g'`
			destination_full=`echo $tv_shows$saison/${destination_dir}`
			dir_to_create=`echo /volume2/series/$destination_full`

			if [ -d "${destination_full}" ]; 
				then echo "${destination_full} already exists" > /dev/null 2>&1;
				else
				echo "Creation of the file ${dir_to_create}" > /dev/null 2>&1;
				mkdir "$dir_to_create"
			fi

			if [ -d "new_episode/$html" ]; 
				then echo "File already exists" > /dev/null 2>&1;
				else
				echo "File doesn't exist - Creation" > /dev/null 2>&1;
				mkdir new_episode/$html 
			fi

			> new_episode/${namePageEpisode}_pages_links_with_priorities.txt
			
			
			# Remarque importante pour les priorités de qualité ! Faut changer l'ordre ici aussi ! 
			# Pas utile du tout ! A voir pour supprimer
			for episodePagesLinks in `cat new_episode/${namePageEpisode}_pages_links.txt`
			do
				echo $episodePagesLinks | grep '720p' >> new_episode/${namePageEpisode}_pages_links_with_priorities.txt
			done
			
			for episodePagesLinks in `cat new_episode/${namePageEpisode}_pages_links.txt`
			do

				echo $episodePagesLinks | grep '1080p' >> new_episode/${namePageEpisode}_pages_links_with_priorities.txt
			done
			
			for episodePagesLinks in `cat new_episode/${namePageEpisode}_pages_links.txt`
			do
				echo $episodePagesLinks | grep -v '480p\|720p\|1080p' >> new_episode/${namePageEpisode}_pages_links_with_priorities.txt
			done
			#############################################################################
			
			nbrPagesLinks=0;
			for episodePagesLinks in `cat new_episode/${namePageEpisode}_pages_links_with_priorities.txt`
			do
				nbrPagesLinks=`expr $nbrPagesLinks + 1`
				# echo $episodePagesLinks
				quality=`echo $episodePagesLinks | egrep -o '(720p|1080p|hdtv|720p.*hdtv)'`
				episodePagesLinksParam=`echo $nbrPagesLinks-$namePageEpisode-$quality.html`
				# wget -qO new_episode/$html/$episodePagesLinksParam $base$episodePagesLinks
				curl -s $base$episodePagesLinks  -H "$cookie" -H "$user_agent" > new_episode/$html/$episodePagesLinksParam

			done


			boolDLLaunched=0;
			
			for namePageEpisodeWithQuality in `ls new_episode/$html/*.html | sort `
			do
				if [ "$boolDLLaunched" = "1" ]
				then 
				break
				fi
			
				> new_episode/$html/uploaded_links.txt
				
				# echo ${namePageEpisodeWithQuality}
				echo ${namePageEpisodeWithQuality} | grep -Eo 'hdtv|720p|1080p|720p.*hdtv'
				# Le ul_link cherche des liens soit de la forme "uploaded.net/file/blabla" soit de la forme "ul.to/blabla"
				# sed -n '/<pre class=\"links\"/,/<\/pre>/p' $namePageEpisodeWithQuality | sed -n '/<div\ style=\"position:relative\">/,/<div\ class=\"clear\"><\/div>/p' | grep -Eo 'http://(ul.to/|uploaded.net/file/)[a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9]' | sort | uniq >> new_episode/$html/uploaded_links.txt
				# Nouvelle façon de trouver les liens uploaded - À voir à l'utilisation si tout fonctionne bien... 
				sed -n '/<pre class=\"links\"/,/<\/pre>/p' $namePageEpisodeWithQuality | grep -Eo 'http://(ul.to/|uploaded.net/file/)[a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9]' | sort | uniq >> new_episode/$html/uploaded_links.txt
				# | sed -e 's/>//g' | sed -e 's/<//g'
				upLinkNbr=0;
				for links_to_dl in `cat new_episode/${html}/uploaded_links.txt`
				do 
					# echo $links_to_dl
					upLinkNbr=`expr $upLinkNbr + 1`
					wget -qO new_episode/$html/page_uploaded_$upLinkNbr.txt $links_to_dl
				done

				# > new_episode/$html/nbrLignesPagesUploaded.txt
				# for uploadedPages in `ls new_episode/$html/page_uploaded*`
				# do
					# cat $uploadedPages | wc -l >> new_episode/$html/nbrLignesPagesUploaded.txt
				# done 
				# nbrUnavailableLinks=`cat new_episode/$html/nbrLignesPagesUploaded.txt | grep  0 | wc -l`
				# # nbrUnavailableLinks -> Booléen: si =0 alors tous les liens sont ok !


				# if [ "$nbrUnavailableLinks" = "0" ];
				# then
					# echo "All link(s) valid"
				# else
					# echo 'Some link(s) INVALID' 
				# fi
				
				
				### Autre facon pour check validité des liens ###
				> new_episode/$html/http_codes.txt
				for links in `cat new_episode/$html/uploaded_links.txt`
				do 
					curl -sL $links --write-out %{http_code} --output /dev/null >> new_episode/$html/http_codes.txt
					echo -e "\n" >> new_episode/$html/http_codes.txt
				done

				bool_valid=`cat new_episode/$html/http_codes.txt | grep -e '404\|410' | wc -l`
				nbrUnavailableLinks='1'
				if [ "$bool_valid" = "0" ]
				then
					echo All links valid
					nbrUnavailableLinks='0'
					# echo "0" > nbr_links_invalid.txt
				else 
					# echo "1" > nbr_links_invalid.txt
					echo Some links invalid
				fi
				#################################################
				

				
				for links_to_dl in `cat new_episode/$html/uploaded_links.txt`
				do
					dest=`echo series/$tv_shows$saison/${destination_dir} | sed -e 's/\ /\%20/g'`
					destinationDirWithUnderscore=`echo $destination_dir | sed -e 's/\ /_/g'`
					boolDlDoing=`cat new_episode/destinations_downloading.txt | grep "$destinationDirWithUnderscore" | wc -l`

					if [ "$nbrUnavailableLinks" = "0" ];
					then
						if [ "$boolDlDoing" = "0" ]
						then
								echo $links_to_dl - NOT downloading - Task added !
								boolDLLaunched="1";
								#### Requette pour lancer les DL
								curl -k "http://127.0.0.1:5000/webapi/DownloadStation/task.cgi?api=SYNO.DownloadStation.Task&version=1&method=create&uri=$links_to_dl&destination=$dest&_sid=$sid"
						else 
							echo $links_to_dl - Downloading - NOT added task
						fi

					else
						echo $links_to_dl - NOT added task > /dev/null 2>&1
					fi
				done
			done
		fi
		else
		echo "Don't want to update that show"
	fi
done  

# rétablissement du séparateur de champ par défaut
IFS=$old_IFS     

cd new_episode

### On se deloggue à la fin du script (sécurité, sid plus valide...)
curl -k "http://127.0.0.1:5000/webapi/auth.cgi?api=SYNO.API.Auth&version=1&method=logout&session=DownloadStation"
###################################################################

### Suppression fichiers temp
if ls *.html > /dev/null 2>&1 ; 
then 
	echo "html file(s) -- DELETING"
	rm *.html; 
	else echo "No html file(s)" > /dev/null 2>&1  ; 
fi

if ls */*.html > /dev/null 2>&1 ; 
then 
	rm */*.html; 
fi

if ls *.txt > /dev/null 2>&1 ; 
then 
	echo "txt file(s) -- DELETING" > /dev/null 2>&1
	rm *.txt; 
	else echo "No txt file(s)" > /dev/null 2>&1 ; 
fi

if ls */*.txt > /dev/null 2>&1 ; 
then 
	rm */*.txt;
fi

if ls -d */ > /dev/null 2>&1 ;
then
	rmdir */
fi
############################

# Suppression des fichiers "parasites" qui arrivent pendant le DL
cd ..
if ls */*/*.url > /dev/null 2>&1;
then
	rm */*/*.url
fi

if ls */*/*/*.url > /dev/null 2>&1;
then
	rm */*/*/*.url
fi
cd new_episode
#################################################################


date -I'seconds' | sed -e 's/T/\ /g' | sed -e 's/+.*$//g'
###

################################################### Fin script - requêttes kodi et To Do List #################################################################
##### URL's à utiliser pour la partie avec KODI ######
# Pour lancer la synchro kodi: 
# curl --data-binary '{ "jsonrpc": "2.0", "method": "VideoLibrary.Scan", "id": "mybash"}' -H 'content-type: application/json;' http://192.168.1.72:8080/jsonrpc
#
# Pour clear meiatheque kodi: 
# curl --data-binary '{ "jsonrpc": "2.0", "method": "VideoLibrary.Clean", "id": "mybash"}' -H 'content-type: application/json;' http://192.168.1.72:8080/jsonrpc
######################################################


##### Jusque là : 
### - Avant de commencer, le script regarde les DL dans series qui sont finis, et notifie (vis pushbullet) les DL pour un même fichier
### - Le script voit à quel épisode on en est et lequel il doit dl
### - trouve l'url de la page de l'épisode qui nous intéresse en 720p et hébergé sur uptobox
### - il affiche ce qu'il fait (lancer le prog avec | tee -a fich.log pour creer un fichier log de l'éxecution)
### - il supprime tous les .html/.txt téléchargés au début pour le traitement, de façon à laisser la place libre pour la prochaine exécution
### - il lance le dl de l'épisode n+1 dans le dossier "series/tv_shows/saison/tv_shows SNEN" pour chaque épisode (même si le dl est composé de plusieurs liens)
### - il place l'épisode dans un dossier du nom comme il faut (sans renommer le fichier lui même) mais peut suffir, A TESTER !
### - Il fait la différence entre un dossier de l'épisode N+1 qui est vide ou pas !
### - Il détecte quand TOUS les liens sont valides ou pas : 
### 	Si valides -> Affiche: TOUS les liens sont valides
###       Si un DL est PAS en cours dans le dossier de réception -> ajoute TOUS les DL et affiche pour chaque lien: DL ajouté
###       Si un DL EST en cours dans le dossier de réception -> N'ajoute PAS les DL
###     Si PAS valide -> Affiche: Certains liens sont invalides, Affiche que les DL ne sont pas ajoutés.
###     POUR AMELIORER -> vérifier que TOUS les liens pour un meme DL sont ok ! (car pour le moment ne fait pas la différence)
### - Lance actualisation de kodi si il y a eu au moins 1 épisode de DL (sinon rien)
### - Supprime les taches de DL finies après les avoir notifié
### - Si les liens ne sont pas valides, il en essaie d'autres (par ordre de préférence -> 720p (du plus gros au plus petit), 1080p, HDTV) | il exclue les 480p
### - Sélectionne à qui il envoie des notifications de téléchargements terminés en fonction de ce qui est présent dans les .dat (gab ou ghys)
### - Supprime les fichiers "parasites" dus au DL
### - Changement dans la façon dont les notifications s'affichent -> titre de la series lu dans le dossier "series", num de la saison dans les dossiers saisons et le numero dans le dernier dossier
### - ATTENTION -> Rapidmoviez est protégé par Cloudflare (contre DDoS attaques) -> Utilisation de Cloudflare-scrape en python - dans bypass_cloudflare.py | Nouvelle session à chaque fois
### - Nouvelle façon de checker la validité des liens, plus robustes, regarde uniquement les codes HTTP des pages des liens (plus rapide aussi)
### - Maj du script pour coller avec la nouvelle API de DownloadStation fournie avec DSM 6
### - Sélection des épisodes même lorsqu'il n'y en que dans la catégorie "Archived"
#####

##### Reste à faire : 
### - IMPORTANT ! -> Faire en sorte que le script connaisse la qualité de l'épisode n et regarde si meilleure qualité dispo pour le n+1 et DL si oui ! 
### - Pourrait aussi faire en sorte qu'il télécharge les sous-titres automatiquement
### - Pourrait faire en sorte que quand une nouvelle saison est dispo il DL pour la nouvelle saison
### - Pourrait aussi renommer les fichier qu'il a DL de la meme facon que le dossier parent (avec la bonne extension ATTENTION !)
### - Comprendre pourquoi Kodi ne télécharge pas l'image de la saison quand on change de saison...
### - Faire quelque chose de plus visuel pour voir quelles séries sont en "update" et pour voir à "qui elles appartiennent"
###############################################################################################################################################################
